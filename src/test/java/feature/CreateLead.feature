Feature: Create Lead for Leaftap
Scenario: Positive Create Lead flow
Given open the browser
And maximize the browser
And load the url
And Enter the username as DemoSalesManager
And Enter the password as crmsfa
And Click on the login button
And Click the CRMSFA link
And Click the Leads link
And Click the CreateLead button
And Enter the Companyname as TCS
And Enter the FirstName as suvetha
And Enter the LastName as vasanthakumar
When Click Create Lead button
Then Verify Lead is created