package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC005_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_DeleteLead";
		testcaseDec = "Deleting a Lead";
		author = "suvetha";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void duplicateLead(String uName, String uPassword) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(uPassword)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.clickFindLeadButton()
		.clickFirstSearchResult()
		.clickDuplicateButton()
		.createDuplicateLead();
		
		
		
		
	}
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







