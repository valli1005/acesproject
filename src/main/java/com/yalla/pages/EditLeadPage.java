package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class EditLeadPage extends Annotations{ 

	public EditLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.ID, using="updateLeadForm_companyName") WebElement eleEditCompanyName;
	@FindBy(how=How.ID, using="updateLeadForm_firstName") WebElement eleEditFirstName;
	@FindBy(how=How.ID, using="updateLeadForm_lastName") WebElement eleEditLastName;
	@FindBy(how=How.XPATH, using="//input[@value = 'Update']") WebElement eleUpdateButton;
	
	public EditLeadPage editCompany(String data) {
		
		clearAndType(eleEditCompanyName, data);  
		return this; 
	}
	
	public EditLeadPage editFirstname(String data) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleEditFirstName, data); 
		return this; 
	}

	public EditLeadPage editLastname(String data) {
		//WebElement elePassWord = locateElement("id", "password");
		clearAndType(eleEditLastName, data); 
		return this; 
	}
	
	public ViewLeadPage clickUpdateButton() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleUpdateButton);  
		return new ViewLeadPage();

	}

	
}







